#!/bin/bash

#Baixar vídeo do youtube ou outro lugar
#Video em MKV sem compressão
#Fonte: http://www.divx.com/en/devices/profiles/video

if ! test -f WiegelesHeliSki_DivXPlus_19Mbps.mkv
then
    echo "Baixando video para teste"
    perf stat -B -d --output download1.out wget "http://trailers.divx.com/divx_prod/profiles/WiegelesHeliSki_DivXPlus_19Mbps.mkv"
fi


#Redimensionamento de vídeo

#Comando executa 1 threads
for i in $(seq 1 5)
do
echo "Redimensionando video usand 1 Thread $i:"
perf stat -B -d --output timeResize1Thread$i.out mencoder WiegelesHeliSki_DivXPlus_19Mbps.mkv -oac copy -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell -really-quiet -vf scale=640:480 -o sortie1thread.avi
done

#Comando executa até 8 threads
for i in $(seq 2 8)
do
echo "Redimensionando video usand $i Threads:"
perf stat -B -d --output timeResizeThreadsN$i.out mencoder WiegelesHeliSki_DivXPlus_19Mbps.mkv -oac copy -ovc lavc -lavcopts vcodec=mpeg4:mbd=2:trell:threads=$i -really-quiet -vf scale=640:480 -o sortie1thread.avi
done

#Gerar imagens a partir do vídeo

#Fazer cópias da imagem(Teste de Disco)
