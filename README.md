## Projeto 1

Grupo 11

Programa de benchmark: Mencoder/MPlayer com transferência do via rede (usando scp) do arquivo na saída.

## O que faz? Para que serve?
  
Mencoder/MPlayer é uma ferramenta para codificação de vídeos. Usado para manipular e converter arquivos de áudio e vídeo.
  
## Por que ele bom para medir desempenho?
  
Mencoder/MPlayer é um bom programa para medir o desempenho, porque ele utiliza de um processamento pesado para tratar vídeos. Ele utiliza a CPU, memória, cache e disco. Como ele não utiliza rede, o uso da transferência via rede permitirá o teste de I/O.
  
## O que baixar
  
Todas as bibliotecas e pacotes necessários estão listados na seção abaixo, além das instruções de como instalá-los
  
## Como compilar/instalar
  
Os programas que serão utilizados, juntamente com seus pacotes, estão disponíveis no respositórios das distribuições de linux.Para Ubuntu e Debian pode-se usar os seguintes comandos para instalar o mencoder:
  
`$ sudo apt-get update`  
`$ sudo apt-get install mencoder`  
  
O programa perf, que será utilizado para gerar as estatísticas do programa que será executado, está disponível no pacote linux-tools-generic. Para instalá-lo use os seguintes comandos:
  
`sudo apt-get install linux-tools-generic`
`sudo apt-get install linux-tools-4.4.0-64-generic`
  
## Como executar
  
Para executar o programa e obter as medidas de perfórma-se, basta executa o script exec.sh no diretório raiz do sistema. Segue a linha de comando:
  
`bash exec.sh`
  
O resultado das medidas de desempenho estarão nos arquivos do formato .out
## Como medir o desempenho
  
O desempenho será medido através da redimensionalização de um vídeo no formato mkv e, subsequentemente, sua conversão para o formato avi.
O script fará esta tarefa 5 vezes usando uma thread e depois executará uma vez usando diferente números de threads (2 a 8 threads).
Para cada execução o programa perf gerará um relatório estatístico com as medidas de desempenho.
  
A principal análise será feita no tempo de execução utilizando uma thread somente. Este tempo engloba todo o processo de redimensionalização e conversão do vídeo.
O tempo médio será a média aritmética simples dos tempos dessas 5 execuções.
  
Outras medidas serão igualmente levadas em consideração, como o número de instruções por ciclo, o número de cache misses e o número de page faults.
O número de ciclos pode dizer o quão eficiente foi a execução dos instruções do programa, enquanto que o número de cache misses e de page faults podem dizer mais sobre a utilização das memórias.

  
## Como apresentar o desempenho
  
As medidas de desempenho que serão apresentadas para esse programa são
1. Tempo de execução da redimensionalização e conversão do vídeo
2. Número de instruções por ciclo
3. Número de cache misses do cache L1
4. Número de Page Faults

  
## Medições base (uma máquina)
  
Usaremos uma máquina base com as seguintes configurações:
 * OS: Linux Ubunt 16.04 LTS
 * Arquitetura: x86_64
 * Processador: Intel® Pentium(R) CPU N3710 @ 1.60GHz × 4
 * Memória: 4GB 1600MHz
 * Disco: 465GB 5200 RPM


Inclua a especificação dos componentes relevantes e os resultados de desempenho.
